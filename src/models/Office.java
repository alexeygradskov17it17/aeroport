package models;

public class Office {
    private int ID;
    private int countryID;
    private String title;
    private String phone;
    private String contact;

    public Office(int ID,
                  int countryID,
                  String title,
                  String phone,
                  String contact){
        this.ID = ID;
        this.countryID = countryID;
        this.title = title;
        this.phone = phone;
        this.contact = contact;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
