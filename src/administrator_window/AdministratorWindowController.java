package administrator_window;

import Database.Database;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import models.Office;

import java.util.ArrayList;
import java.util.List;

public class AdministratorWindowController {

    public Text btnAddUser;
    public Text btnExit;
    public ComboBox<String> comboBox;

    public void initialize() {
        List<Office> listOfOffices = Database.getInstance().getAllOffices();
        List<String> listOfTitles = new ArrayList<>();
        for (Office office :
                listOfOffices) {
            listOfTitles.add(office.getTitle());
        }
        ObservableList<String> titles = FXCollections.observableArrayList(listOfTitles);
        comboBox.setItems(titles);
        comboBox.setValue(titles.get(0));

        comboBox.valueProperty().addListener((ov, t, t1) -> {

        });
    }
}
