package AutorizationWindow;

import Database.Database;
import exceptions.InvalidPasswordException;
import exceptions.NonExistentLoginException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;
import models.User;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.List;

public class AuthorizationWindowController {
    public TextField loginTF;
    public TextField passwordTF;
    public ImageView imageView;
    public Button btnExit, btnLogin;

    double displayWidth = 500;

    int countOfAttempt = 0;


    public void initialize() {
        File file = new File("src/Images/DS2017_TP09_color.png");
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
        imageView.setX((displayWidth - image.getWidth()) / 2);
        Database database = Database.getInstance();

        btnLogin.setOnAction(event -> {
            if (!loginTF.getText().equals("Login don't exist")){
            try {
                User user = database.getUserByLogin(loginTF.getText());
                System.out.println(passwordTF.getText() + " "+user.getPassword());
                if (user.getPassword().equals(passwordTF.getText())) {
                    Stage stage = (Stage) imageView.getScene().getWindow();
                    Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("administrator_window\\administrator_window.fxml"));
                    stage.setTitle("AMONIC Airlines Automation System");
                    stage.setScene(new Scene(root, 500, 400));
                } else {
                    throw new InvalidPasswordException(passwordTF.getText());
                }
            } catch (NonExistentLoginException e) {
                loginTF.setText("Login don't exist");
                countOfAttempt++;
                if (countOfAttempt==3){
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                }
            } catch (InvalidPasswordException e) {
                passwordTF.setText("Password is not correct");
                countOfAttempt++;
                if (countOfAttempt==3){
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
        });

        btnExit.setOnAction(event -> {
            Stage stage = (Stage) imageView.getScene().getWindow();
            stage.close();
        });

    }

    public AuthorizationWindowController() throws SQLException {
    }
}
