package exceptions;

public class NonExistentLoginException extends Exception{
    private String login;

    public NonExistentLoginException(String _login){
        login = _login;
    }

    public String toString() {
        String msg = "Exception: " + login + " not exist";
        return msg;
    }
}
