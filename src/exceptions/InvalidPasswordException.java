package exceptions;

public class InvalidPasswordException extends Exception {
    private String password;

    public InvalidPasswordException(String _password) {
        password = _password;
    }

    @Override
    public String toString() {
        return "InvalidPasswordException:" +
                "password='" + password +
                "don't correct";
    }
}
