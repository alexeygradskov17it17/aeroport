package Database;

import exceptions.NonExistentLoginException;
import models.Office;
import models.User;

import javax.jws.soap.SOAPBinding;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class Database {
    private static Database instance;
    public static Connection connection;
    public static Statement statement;

    static String instanceName = "DESKTOP-T71AMPQ\\DATA";
    static String databaseName = "aeroport_data";
    static String userName = "sa";
    static String pass = "gradskov";
    static String connectionUrl = "jdbc:sqlserver://%1$s;databaseName=%2$s;user=%3$s;password=%4$s;";
    static String connectionString = String.format(connectionUrl, instanceName, databaseName, userName, pass);

    public Database() {
    }


    public static Database getInstance() {
        if (statement == null) {
            try {
                instance = new Database();
                connection = DriverManager.getConnection(connectionString);
                statement = connection.createStatement();
                return instance;
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        return instance;
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Users");
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getDate(8),
                        resultSet.getInt(9)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return users;
    }

    public User getUserByLogin(String login) throws NonExistentLoginException {
        User user = new User(0, 0, 0, "", "", "", "", new Date(2021 - 1 - 1), 0);
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Users WHERE email='" + login + "'");
            if (!resultSet.isBeforeFirst()) {
                throw new NonExistentLoginException(login);
            } else {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getDate(8),
                            resultSet.getInt(9));
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return user;
    }

    public List<Office> getAllOffices() {
        List<Office> offices = new ArrayList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Offices");
            while (resultSet.next()) {
                offices.add(new Office(
                        resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return offices;
    }
}
